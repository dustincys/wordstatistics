﻿package fastaStatistics;
import java.io.BufferedWriter;
import statistics.TextAreaMenu;
public class FastaStatistics extends csvDataAnalyse.CSVFILE{
    public FastaStatistics(String _url){
        super(_url);
        this.setReset();
    }
    protected T27 tree=null;//树头结点    
    protected String oneline=null;
    
    protected double m_doubleConsistency=40;//一致性(40%)
    protected boolean m_boolEachOrAll=false;//是否分条统计(否)
    protected boolean m_boolSpecificEntry=false;//统计方式(普遍统计)
    protected int m_intEntryLen=1;//统计词条长度(1)
    protected String m_strSpecificEntry=null;//特定词条(null)
    
    protected int m_intFastaItemNum=0;//文件词条数目
    protected int m_intOccurrence=0;//特定词条的频数
    protected double m_intMeanFrequence=0;//特定词条的频率
    
    public T27 getTree(){
        return this.tree;
    }
    public int getFastaItemNum(){
        return this.m_intFastaItemNum;
    }
    public int getOccurrence(){
        return this.m_intOccurrence;
    }
    public double getMeanFrequence(){
        this.m_intMeanFrequence=(double)this.m_intOccurrence/this.m_intFastaItemNum;
        return Math.round(this.m_intMeanFrequence*100)/100.0;
    }
    public void setdoubleConsistency(double value){
        this.m_doubleConsistency=value;
    }
    public void setboolEachOrAll(boolean value){
        this.m_boolEachOrAll=value;
    }
    public void setboolSpecificEntry(boolean value){
        this.m_boolSpecificEntry=value;
    }
    public void setintEntryLen(int value){
        this.m_intEntryLen=value;
    }
    public void setstrSpecificEntry(String value){
        this.m_strSpecificEntry=value;
    }
    public void setReset(){
        tree=new T27();//树头结点    
        oneline=null;        
        m_doubleConsistency=40;//一致性(40%)
        m_boolEachOrAll=false;//是否分条统计(是)
        m_boolSpecificEntry=false;//统计方式(普遍统计)
        m_intEntryLen=1;//统计词条长度(1)
        m_strSpecificEntry=null;//特定词条(null)
        m_intFastaItemNum=0;//文件词条数目
        m_intOccurrence=0;//特定词条的频数
        m_intMeanFrequence=0;//特定词条的频率
    }
    public void showResult(TextAreaMenu _tam){//输出结果
        _tam.append(this.tree.print_tree());
    }
    
    public void solution(){//通篇统计
        try{
            if(this.m_boolSpecificEntry){//统计方式 是针对特定词条的统计
                if(this.m_strSpecificEntry.equals("")){
                    return;//空词条容错
                }
                this.specializedStatistics();
            }else{//不针对特定词条的统计
                this.unspecializedStatistics();                
            }            
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    public void solution(TextAreaMenu tam, BufferedWriter bw){//分词条同步统计
        try{
            if(this.m_boolSpecificEntry){//统计方式 是针对特定词条的统计
                if(this.m_strSpecificEntry.equals("")){
                    return;//空词条容错
                }
                this.specializedStatistics(tam,bw);
            }else{//不针对特定词条的统计
                this.unspecializedStatistics(tam,bw);
            }            
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    protected void specializedStatistics(TextAreaMenu tam, BufferedWriter bw) throws Exception{
        String lastlineentry="";
        int numEntryInOneItem=0;
        while((this.oneline=this.get_oneline())!=null){
            this.oneline=this.oneline.toLowerCase();
            if(this.oneline.equals("")){
                continue;
            }
            if(this.oneline.contains(">")){//fasta文件头
                if(m_intFastaItemNum>0){
                    tam.append(numEntryInOneItem+"\r\n");
                    bw.write(numEntryInOneItem+"\r\n");
                }
                this.m_intFastaItemNum++;
                lastlineentry="";
                numEntryInOneItem=0;
                tam.append(oneline+"\r\n");
                bw.write(oneline+",");
            }else{
                this.oneline=lastlineentry+this.oneline;//加上上行末尾词条
                if(this.oneline.length()<this.m_strSpecificEntry.length()){
                    lastlineentry=this.oneline;
                    continue;
                }else{
                    lastlineentry=this.oneline.substring(this.oneline.length()-this.m_strSpecificEntry.length()+1,this.oneline.length());
                }
                numEntryInOneItem+=this.specificEntryOccurrenceInOneline();
            }
        }
        tam.append(numEntryInOneItem+"\r\n");
        bw.write(numEntryInOneItem+"\r\n");
    }
    protected void unspecializedStatistics(TextAreaMenu tam, BufferedWriter bw) throws Exception{
        String lastlineentry="";
        T27 tempTree=new T27();
        while((this.oneline=this.get_oneline())!=null){
            this.oneline=this.oneline.toLowerCase();
            if(this.oneline.equals("")){
                continue;
            }
            if(this.oneline.contains(">")){//fasta文件头
                if(this.m_intFastaItemNum>0){
                    tam.append(tempTree.print_tree());
                    bw.write(tempTree.print_tree());
                    tempTree=null;
                }
                this.m_intFastaItemNum++;
                lastlineentry="";
                tam.append(this.oneline+"\r\n");
                bw.write(oneline+"\r\n");
                tempTree=new T27();
            }else{
                this.oneline=lastlineentry+this.oneline;//加上上行末尾词条
                lastlineentry=this.oneline.substring(this.oneline.length()-this.m_intEntryLen+1,this.oneline.length());
                this.analyse_line_unspecializedStatistics(tempTree);
            }
        }
        tam.append(tempTree.print_tree());
        bw.write(tempTree.print_tree());
    }
    protected void specializedStatistics() throws Exception{//普遍统计的特异性统计
        String lastlineentry="";        
        while((this.oneline=this.get_oneline())!=null){
            this.oneline=this.oneline.toLowerCase();
            if(this.oneline.equals("")){
                continue;
            }
            if(this.m_intFastaItemNum==46){
                System.out.println();
            }
            if(this.oneline.contains(">")){//fasta文件头
                this.m_intFastaItemNum++;
                lastlineentry="";
            }else{
                this.oneline=lastlineentry+this.oneline;//加上上行末尾词条
                if(this.oneline.length()<this.m_strSpecificEntry.length()){
                    lastlineentry=this.oneline;
                    continue;
                }else{
                    lastlineentry=this.oneline.substring(this.oneline.length()-this.m_strSpecificEntry.length()+1,this.oneline.length());
                }
                this.analyse_line_specializedStatistics();
            }
        }
    }
    protected void unspecializedStatistics() throws Exception{
        String lastlineentry="";
        while((this.oneline=this.get_oneline())!=null){
            this.oneline=this.oneline.toLowerCase();
            if(this.oneline.equals("")){
                continue;
            }
            if(this.oneline.contains(">")){//fasta文件头
                this.m_intFastaItemNum++;
                lastlineentry="";
            }else{
                this.oneline=lastlineentry+this.oneline;//加上上行末尾词条
                lastlineentry=this.oneline.substring(this.oneline.length()-this.m_intEntryLen+1,this.oneline.length());
                this.analyse_line_unspecializedStatistics();
            }
        }
    }
    protected void analyse_line_specializedStatistics(){
        if(!this.oneline.contains(this.m_strSpecificEntry))
            return;
        this.m_intOccurrence++;
        for(int i=this.oneline.indexOf(this.m_strSpecificEntry)+1;i<=this.oneline.length()-this.m_strSpecificEntry.length();i++){
            if(this.oneline.length()-1<this.m_strSpecificEntry.length()){
                return;
            }else if(this.oneline.substring(i,i+this.m_strSpecificEntry.length()).equals(m_strSpecificEntry)){
                this.m_intOccurrence++;
            }
        }
    }
    protected int specificEntryOccurrenceInOneline(){
        if(!this.oneline.contains(this.m_strSpecificEntry))
            return 0;
        this.m_intOccurrence++;
        int temp=0;
        for(int i=this.oneline.indexOf(this.m_strSpecificEntry)+1;i<=this.oneline.length()-this.m_strSpecificEntry.length();i++){
            if(this.oneline.length()-1<this.m_strSpecificEntry.length()){
                return temp;
            }else if(this.oneline.substring(i,i+this.m_strSpecificEntry.length()).equals(m_strSpecificEntry)){
                this.m_intOccurrence++;
                temp++;
            }
        }
        return temp;
    }
    protected void analyse_line_unspecializedStatistics()
    {        
        if(this.oneline.length()==0)
            return;
        String a_word=null;
        int i,j;
        for(i=0;i<=this.oneline.length()-this.m_intEntryLen;i++){            
            a_word=this.oneline.substring(i,i+this.m_intEntryLen).toLowerCase();
            if(!acceptConsistency(a_word)){//一致性监测
                continue;
            }
            for(j=0;j<a_word.length();j++){
                if(!this.isEChar(a_word.charAt(j))){
                    break;
                }
            }
            if(j<a_word.length()){
                continue;//如果出现非法字符则继续
            }
            tree.add2Tree_compressed(a_word);            
        }
    }
    protected void analyse_line_unspecializedStatistics(T27 tempTree)
    {        
        if(this.oneline.length()==0)
            return;
        String a_word=null;
        int i,j;
        for(i=0;i<=this.oneline.length()-this.m_intEntryLen;i++){            
            a_word=this.oneline.substring(i,i+this.m_intEntryLen).toLowerCase();
            if(!acceptConsistency(a_word)){//一致性监测
                continue;
            }
            for(j=0;j<a_word.length();j++){
                if(!this.isEChar(a_word.charAt(j))){
                    break;
                }
            }
            if(j<a_word.length()){
                continue;//如果出现非法字符则继续
            }
            tempTree.add2Tree_compressed(a_word);            
        }
    }
    protected void analyse_line_unspecializedStatistics(TextAreaMenu tam, BufferedWriter bw)
    {        
        if(this.oneline.length()==0)
            return;
        String a_word=null;
        int i,j;
        for(i=0;i<=this.oneline.length()-this.m_intEntryLen;i++){            
            a_word=this.oneline.substring(i,i+this.m_intEntryLen).toLowerCase();
            if(!acceptConsistency(a_word)){//一致性监测
                continue;
            }
            for(j=0;j<a_word.length();j++){
                if(!this.isEChar(a_word.charAt(j))){
                    break;
                }
            }
            if(j<a_word.length()){
                continue;//如果出现非法字符则继续
            }
            tree.add2Tree_compressed(a_word);            
        }
    }
    protected boolean acceptConsistency(String a_word){
        if(this.m_doubleConsistency<0||this.m_doubleConsistency>100){
            this.m_doubleConsistency=40;
        }
        char tempChar='#';
        int numOfTempChar=0;
        for(int i=0;i<a_word.length();i++){
            if(tempChar!=a_word.charAt(i)){//清空记录
                tempChar=a_word.charAt(i);
                numOfTempChar=0;
            }else{
                numOfTempChar++;
                if(numOfTempChar>=((double)a_word.length()*this.m_doubleConsistency/100.0)){
                    return false;
                }else if((numOfTempChar-i+a_word.length()-1)<((double)a_word.length()*this.m_doubleConsistency/100.0)){
                    return true;
                }                
            }
        }
        return true;
    }
    protected boolean isEChar(char c){
        if( (('a'<=c)&&(c<='z'))    ||  (('A'<=c)&&(c<='Z'))    ){
            return true;
        }else{
            return false;
        }
    }
}
