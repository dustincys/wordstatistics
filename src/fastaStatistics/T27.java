﻿package fastaStatistics;

public class T27{
    private T27node head=null;//树头结点    
////////////////////add2Tree_compressed 函数参数//////////////////
    private T27node neareat_ontop=null;//返回上面最近的结点
    private int depth_of_neareast_ontop=-1;//返回上面最近结点的梯度
    static private enum Relationship {BOTHER,MYSELF,D_ANCESTOR,B_ANCESTOR,D_OFFSPRING,B_OFFSPRING};
    private Relationship m_rs_Of_a_word;//关系
////////////////////////////////////////////////////////////////    
    public T27(){
        this.head=new T27node();
    }
    public String print_tree(){
        return this.printT(this.head);
    }
    private String printT(T27node p){
        if(p==null)
            return "";
        String result="";
        for(int i=0;i<27;i++){
            if(p.child[i]==null)
                continue;
            result+=printT(p.child[i]);
        }
        if(p.frequent>0){
            System.out.println(p.word+"\t"+p.frequent);
            result+=(p.word+","+p.frequent+"\r\n");
        }
        return result;
    }
    private int num_identicalChar(String str1, String str2)
    {
        int i=0;
        for(;i<str1.length()&&i<str2.length();i++){
            if(str1.charAt(i)!=str2.charAt(i))
                break;
        }
        return i-1;
    }
    private boolean locate_tree_compressed(String a_word){
        /*
                    该函数返回三个数值： 1 bool 函数返回的。2 p_temp 指针。3 depth 
                    p_temp、depth 在函数返回true时无用
                    当返回false 时，p_temp指向最接近a_word的a_word的祖先，depth表示其深度
        */
        T27node p_temp=this.head;
        T27node temp_pointer=null;
        //cout<<a_word<<endl;
        //cout<<"a_word.length()="<<a_word.length()<<endl;
        //if(a_word=="you")
//        //  cout<<"wait";
//        if(a_word.equals("b"))
//            System.out.println();
        for(int i=0;i<a_word.length();){
            //cout<<"a_word.charAt(i)="<<a_word.charAt(i)<<endl;
            //cout<<"T27node.mapchar(a_word.charAt(i))="<<T27node.mapchar(a_word.charAt(i))<<endl;
            temp_pointer=p_temp.child[T27node.mapchar(a_word.charAt(i))];
            if(temp_pointer==null){
                //找到路径上的null
                this.neareat_ontop=p_temp;         
                this.depth_of_neareast_ontop=i;
                this.m_rs_Of_a_word=Relationship.D_ANCESTOR;//是直系祖先
                return false;
            }else{
                if(temp_pointer.word.length()>a_word.length()){
                    //是直系后代，或者旁系后代
                    if(temp_pointer.word.substring(0,a_word.length()).equals(a_word)){
                        //是直系后代，新加入链条
                        this.neareat_ontop=p_temp;
                        this.depth_of_neareast_ontop=i;
                        this.m_rs_Of_a_word=Relationship.D_OFFSPRING;
                        return false;           
                    }else{
                        //是旁系后代，生成最近共同祖先为根的空字树
                        this.neareat_ontop=p_temp;
                        this.depth_of_neareast_ontop=i;
                        this.m_rs_Of_a_word=Relationship.B_OFFSPRING;
                        return false;
                    }

                }else if(temp_pointer.word.length()<a_word.length()){
                    //还是直系祖先,或是旁系祖先
                    if(a_word.substring(0,temp_pointer.word.length()).equals(temp_pointer.word)){
                        //是直系祖先
                        p_temp=p_temp.child[T27node.mapchar(a_word.charAt(i))];
                        i=temp_pointer.word.length();
                        continue;
                    }else{
                        //是旁系祖先，生成最近共同祖先为根的空字树
                        this.neareat_ontop=p_temp;
                        this.depth_of_neareast_ontop=i;
                        this.m_rs_Of_a_word=Relationship.B_ANCESTOR;
                        return false;
                    }
                }else{
                    //是兄弟，或者是本身
                    if(temp_pointer.word.equals(a_word)){
                        //是本身
                        this.neareat_ontop=temp_pointer;
                        this.depth_of_neareast_ontop=i;
                        this.m_rs_Of_a_word=Relationship.MYSELF;
                        return true;
                    }else{
                        //是兄弟
                        this.neareat_ontop=p_temp;
                        this.depth_of_neareast_ontop=i;
                        this.m_rs_Of_a_word=Relationship.BOTHER;
                        return false;
                    }
                }           
            }       
        }//for       
        return false;            
    }    
    public boolean add2Tree_compressed(String a_word){
        T27node p_temp_new=null;
        T27node p_temp_anothertree=null;
        int idcal_position=-1;
        if(this.locate_tree_compressed(a_word)){
            this.neareat_ontop.frequent++;
            if(this.neareat_ontop.word.equals(""))
                this.neareat_ontop.word=a_word;            
        }else{
            switch(this.m_rs_Of_a_word){
            case BOTHER:
                p_temp_anothertree=
                    this.neareat_ontop.child[T27node.mapchar(a_word.charAt(this.depth_of_neareast_ontop))];//存储兄弟树
                p_temp_new=
                    new T27node(this.neareat_ontop,a_word.charAt(this.depth_of_neareast_ontop));
                p_temp_new.frequent=0;
                idcal_position=this.num_identicalChar(a_word,p_temp_anothertree.word);
                p_temp_new.word=a_word.substring(0,idcal_position+1);
                p_temp_new.child[T27node.mapchar(p_temp_anothertree.word.charAt(idcal_position+1))]
                    =p_temp_anothertree;
                p_temp_new=new T27node(p_temp_new,a_word.charAt(idcal_position+1));
                p_temp_new.frequent=1;
                p_temp_new.word=a_word;
                break;
            case D_ANCESTOR:
                p_temp_new
                    =new T27node(this.neareat_ontop,a_word.charAt(this.neareat_ontop.word.length()));
                p_temp_new.frequent=1;
                p_temp_new.word=a_word;
                break;
            case B_ANCESTOR:
                p_temp_anothertree=
                    this.neareat_ontop.child[T27node.mapchar(a_word.charAt(this.depth_of_neareast_ontop))];//存储兄弟树
                p_temp_new=
                    new T27node(this.neareat_ontop,a_word.charAt(this.depth_of_neareast_ontop));
                p_temp_new.frequent=0;
                idcal_position=this.num_identicalChar(p_temp_anothertree.word,a_word);
                p_temp_new.word=p_temp_anothertree.word.substring(0,idcal_position+1);
                p_temp_new.child[T27node.mapchar(p_temp_anothertree.word.charAt(idcal_position+1))]
                    =p_temp_anothertree;
                p_temp_new=new T27node(p_temp_new,a_word.charAt(idcal_position+1));
                p_temp_new.frequent=1;
                p_temp_new.word=a_word;
                break;
            case D_OFFSPRING:
                p_temp_anothertree=
                    this.neareat_ontop.child[T27node.mapchar(a_word.charAt(this.depth_of_neareast_ontop))];//存储兄弟树
                p_temp_new
                    =new T27node(this.neareat_ontop,a_word.charAt(this.depth_of_neareast_ontop));
                p_temp_new.frequent=1;
                p_temp_new.word=a_word;
                p_temp_new.child[T27node.mapchar(p_temp_anothertree.word.charAt(a_word.length()))]
                    =p_temp_anothertree;
                break;
            case B_OFFSPRING:
                p_temp_anothertree=
                    this.neareat_ontop.child[T27node.mapchar(a_word.charAt(this.depth_of_neareast_ontop))];//存储兄弟树
                p_temp_new=
                    new T27node(this.neareat_ontop,a_word.charAt(this.depth_of_neareast_ontop));
                p_temp_new.frequent=0;
                idcal_position=this.num_identicalChar(p_temp_anothertree.word,a_word);
                p_temp_new.word=p_temp_anothertree.word.substring(0,idcal_position+1);
                p_temp_new.child[T27node.mapchar(p_temp_anothertree.word.charAt(idcal_position+1))]
                    =p_temp_anothertree;
                p_temp_new=new T27node(p_temp_new,a_word.charAt(idcal_position+1));
                p_temp_new.frequent=1;
                p_temp_new.word=a_word;
                break;
            case MYSELF:
                break;
            }
        }
        return true;
    }
    
}
