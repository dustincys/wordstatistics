﻿package fastaStatistics;

public class T27node{
//————————————————————————————————————————————//    
    public String word;//结点字
    public int frequent;//结点频度    
    public T27node[] child=new T27node[27];//孩子结点
//————————————————————————————————————————————//    
    public T27node(){
        this.word="";
        this.frequent=-1;    
    }
    public T27node(String _word, int _frequent){
        this.word=_word;
        this.frequent=_frequent;    
    }
    public T27node(T27node _parentNode, char c){
        _parentNode.child[T27node.mapchar(c)]=this;
        this.word=_parentNode.word+c;
        this.frequent=0;    
    }
    static int mapchar(char c)//
    {
        switch(c){
            case ' ':
                return 0;
            default:
                return (int)(c-'a'+1);
        }
    }
}
