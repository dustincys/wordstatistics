﻿package statistics;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.Date;
import t27.T27;

public class CHSTextStatistics extends EngTextStatistics{
    private ChineseSpelling finder=ChineseSpelling.getInstance();
    public CHSTextStatistics(String url){
        super(url);
    }
    public CHSTextStatistics(){
        super();
    }
    private FileWriter     fw_outfile;
    private BufferedWriter bw_outfile;
    public boolean outputFile(String url){
        try{
            this.fw_outfile=new FileWriter(url);
            this.bw_outfile=new BufferedWriter(this.fw_outfile);
            writeFile();
            this.bw_outfile.close();
            this.fw_outfile.close();
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
        return true;
    }
    private void writeFile() throws Exception{
        this.tree.getCHSInfoCSV(this.bw_outfile);
    }
    public void initial(){
        this.tree=new T27();//新建树
        this.m_intWords=1;
        this.m_isCompress=true;
        this.m_numentries=0;
        this.m_numfilecharacters=0;
        this.m_numtreenodes=0;
        this.m_room=0;
        this.m_time=0;
    }
    
//—————————————————————参数设置接口——————————————————————————————————————————————————//    
    /**
     * @param m_intWord 词条长度
     * @param m_isCompress 是否压缩
     * @param m_isOutputFile 是否输入结果
     * @param m_isDLLSort 是否排序双链表
     */    
    private boolean m_isCompress=true;
    public void setEntryLen(int len){//设置词条长度
        this.m_intWords=len;
    }
    public void setCompress(boolean isornot){
        this.m_isCompress=isornot;
    }
    public void setDLLSort(boolean isornot){
        this.tree.setDLLSort(isornot);
    }
//——————————————————————————————————————————————————————————————————————————————————// 
//—————————————————————算法性能参数接口————————————————————————————————————————————————//
    /**
     *  @param m_time 算法消耗时间
        @param m_room 算法消耗空间
        @param m_numtreenodes 索引树结点数
        @param m_numentries 词条总数
        @param m_numfilecharacters 文件中字数
     */
    
    private long m_time=0;//
    private float m_room=0;//
    private long m_numtreenodes=0;//
    private long m_numentries=0;//
    private long m_numfilecharacters=0;//
//    private long m_numDLLsize=0;
    public long getTime(){
        return this.m_time;
    }
    
    public float getRoom(){
        this.m_room=(float)(this.getNumTreeNodes()*(16+this.m_intWords*2)
                +this.tree.getDLLsize()*this.m_intWords*2)/(1024*1024);
        return this.m_room;
    }
    public long getNumTreeNodes(){
        this.m_numtreenodes=this.tree.getNumAllNodes();
        return this.m_numtreenodes;
    }
    public long getNumEntries(){
        return this.m_numentries;
    }
    public long getFileCharacters(){
        return this.m_numfilecharacters;
    }
//——————————————————————————————————————————————————————————————————————————————————// 
    public void solution(TextAreaMenu tam){        
//        tam.append("\r\n进入solution");
        long timebegin=new Date().getTime();
        try{
            while((this.oneline=this.br_infile.readLine())!=null){
//                tam.append(this.oneline);
                for(int i=0;i<this.oneline.length();i++)
//                    tam.append("\r\n字符"+i+"为"+this.oneline.charAt(i));
                this.oneline=this.getCHSStr(this.oneline);//转换成汉字
//                tam.append("\r\n完成转换汉字"+this.oneline);
                this.m_numfilecharacters+=this.oneline.length();
                this.analyse_line();
//                tam.append("\r\n完成分析行"+this.oneline);
            }
            this.tree.print_tree_CHS(tam);
        }catch(Exception e){
            tam.append("\r\n"+e.toString());
        }
        this.m_time=new Date().getTime()-timebegin;
    }
    protected void analyse_line()
    {
        String a_word_pinyin=null;
        String a_word_chs=null;        
        
        if(this.oneline.length()==0)
            return;
        for(int i=0;i<=this.oneline.length()-this.m_intWords;i++){
//            tam.append("\r\n分析行字符"+i+"为"+this.oneline.charAt(i));            
            a_word_chs=this.oneline.substring(i,i+this.m_intWords);
//            tam.append("\r\n完成this.oneline.substring(i,i+this.m_intWords);");
            a_word_pinyin=this.getPinYinStr(a_word_chs);
//            tam.append("\r\n完成this.getPinYinStr(a_word_chs);");
            if(a_word_pinyin.contains("?")){
                continue;
            }
            if(this.m_isCompress){  
//                tam.append(""+this.m_isCompress);
//                tam.append("\r\na_word_pinyin="+a_word_pinyin);
//                tam.append("\r\na_word_chs="+a_word_chs);                
                this.tree.add2Tree_compressed(a_word_pinyin,a_word_chs);
//                tam.append("\r\n完成this.tree.add2Tree_compressed(a_word_pinyin,a_word_chs);");
                this.m_numentries++;
            }else{
//                tam.append(""+this.m_isCompress);
                this.tree.add2Tree(a_word_pinyin,a_word_chs);
//                tam.append("\r\n完成this.tree.add2Tree(a_word_pinyin,a_word_chs);");
                this.m_numentries++;
            }            
        }
    }
    private String getCHSStr(String str){
        String temp="";
        for(int i=0;i<str.length();i++){
            if(t27.DLinkedListCHS.isChinese(str.charAt(i))){
                temp+=String.valueOf(str.charAt(i));
            }
        }
        return temp;
    }
    private String getPinYinStr(String str){
        this.finder.setResource(str);
        return this.finder.getSpelling();
    }

}
