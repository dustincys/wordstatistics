﻿package statistics;
import t27.T27;

public class EngTextStatistics extends csvDataAnalyse.CSVFILE{

    public EngTextStatistics(String url){
        super(url);
        // TODO Auto-generated constructor stub
    }
    public EngTextStatistics(){
        super();
        // TODO Auto-generated constructor stub
    }
    /**
     * @类成员 oneline 一行字符串；
     * @类成员 noPunctuation 是否有标点；
     * @类成员 tree 树；
     * @类成员 m_intWords 词条长度；
     */    
    protected String oneline=null;
    protected boolean noPunctuation=false;//按标点识别词语
    protected T27 tree=new T27();
    protected int m_intWords=1;
    protected void analyse_line()
    {
        String a_word;
        String templine=null;//存储pop1个word的行
        int numofword=0;
//        int position=0;
        
        if(this.oneline.length()==0)
            return;
        while((this.oneline.length()!=0)&&!(this.oneline.equals(""))){
            a_word="";
            numofword=0;
            while(numofword<this.m_intWords){
                a_word+=this.popWord().toLowerCase();                
                numofword++;
                if(a_word.equals("")){
                    continue;
                }
                if(numofword==1){
                templine=this.oneline;
                }
                if(numofword!=this.m_intWords)
                    a_word+=" ";
            }       
            if(a_word.equals("")){
                break;
            }
//            if(a_word.equals("on"))
//                System.out.println();
            tree.add2Tree_compressed(a_word);
            this.oneline=templine;//pop掉1个word后的oneline
//            position++;
        }       
    }
    
    public String solution(){
        String result="";
        try{
            while((this.oneline=this.br_infile.readLine())!=null){
                this.analyse_line();
            }
            result+=this.tree.print_tree();
        }catch(Exception e){
            e.printStackTrace();
        }
        return result;
    }
    protected boolean isEChar(char c){
        if( (('a'<=c)&&(c<='z'))    ||  (('A'<=c)&&(c<='Z'))    ){
            return true;
        }else{
            return false;
        }
    }
    protected boolean isSpaceChar(char c){
        if( c==' '||c=='\t'){
            return true;
        }else{
            return false;
        }
    }
    protected String popWord(){
        int i=-1,j=-1;
        //cout<<oneline<<endl;
        for(i=0;i<this.oneline.length();i++){
            if(!this.isEChar(this.oneline.charAt(i))&&!isSpaceChar(this.oneline.charAt(i))){
                this.noPunctuation=false;
            }
            if(isEChar(this.oneline.charAt(i))){
                for(j=i+1;j<this.oneline.length();j++){
                    if(!isEChar(this.oneline.charAt(j))){
                        break;
                    }
                }
                break;
            }
        }
        if(i==this.oneline.length() || j==-1){
            this.oneline="";
            return "";
        } 
        String temp=this.oneline.substring(i,j);    
        this.oneline=this.oneline.substring(j,this.oneline.length());
        return temp;
    }

    /**
     * @param args
     */
    
}
