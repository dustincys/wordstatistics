﻿package statistics;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;

public class S_word{
    
    protected FileReader     fr_infile;
    protected BufferedReader br_infile;
    protected FileWriter     fw_outfile;
    protected BufferedWriter bw_outfile;
    
    public S_word(String _infilepath,String _outfilepath){
        try{
            fr_infile=new FileReader(_infilepath);
            br_infile=new BufferedReader(fr_infile);
            fw_outfile=new FileWriter(_outfilepath);
            this.bw_outfile=new BufferedWriter(this.fw_outfile);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void solution(){
        String oneline=null;
        try{
            while((oneline=br_infile.readLine())!=null){
                if(oneline.contains("千载中文网")){
                    System.out.println(oneline);
                    continue;   
                }else{
                    this.bw_outfile.write(oneline+"\n\r");
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
