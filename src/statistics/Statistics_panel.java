﻿package statistics;

import java.awt.GridLayout;
import java.awt.event.*;
import javax.swing.*;
import java.io.*;
import java.text.DecimalFormat;
//import statistics.*;



public class Statistics_panel extends JFrame implements ActionListener {

    public Statistics_panel(){
        super("《基于垃圾信息过滤机制的网络客服系统研究》 演示5.1");
        initial();
    }
    private static final long serialVersionUID = 1L;
    private JPanel              jp;//底板
    private JPanel              jp_parameter;//参数底板
    private JPanel              jp_performance;//性能底板
    private TextAreaMenu        outputtextarea;//输出
    private JLabel              title_output,title_parameter,title_performance;
    private JLabel[]            label_parameter;
    private JScrollPane         sc1;
    private JButton             b_infile;
    private JComboBox           jcb_compress,jcb_sort,jcb_len;
    private String[]            str_isornot={"是","否"};
    private String[]            str_len={"1","2","3","4","5","6","7","8","9","10"};
    private JButton             b_execute;
    private JButton             b_reset;
    private JLabel[]            label_performance;
    private JTextField[]        textfield_performance;          
    private JFileChooser        jfc_infile,jfc_outfile;    
    private JButton             jb_outputfile;    
    private void initial(){
        jfc_infile=new JFileChooser();
        jfc_infile.setFileFilter(new   javax.swing.filechooser.FileFilter(){ 
            public   boolean   accept(File   f){ 
                return   f.getName().toLowerCase().endsWith( ".txt")||f.isDirectory(); 
            } 
            public   String   getDescription(){ 
                return   ".txt"; 
            } 
        });
        jfc_outfile=new JFileChooser();
        jfc_outfile.setFileFilter(new   javax.swing.filechooser.FileFilter(){ 
            public   boolean   accept(File   f){ 
                return   f.getName().toLowerCase().endsWith( ".csv")||f.isDirectory(); 
            } 
            public   String   getDescription(){ 
                return   ".csv"; 
            } 
        });
        this.setBounds(100, 100, 600, 500);
            jp=new JPanel();
            jp.setBounds(0, 0, 600, 500);
            jp.setLayout(null);
            //////////////////////////////////////////////////////
            /***********************\
             * 统计词条显示框部分
            \***********************/
                title_output=new JLabel("统计词条显示框");
                title_output.setBounds(100,30,100,20);
                this.outputtextarea=new TextAreaMenu(){
                    private static final long serialVersionUID = -2308615404205560110L;  
                    public void mouseClicked(MouseEvent e) {  
                        if(e.getButton()==MouseEvent.BUTTON1)
                            selectAll();
                    } 
                };
                sc1=new JScrollPane(this.outputtextarea); 
                sc1.setBounds(10, 50, 280, 400);
                this.outputtextarea.setEditable(false);
            /////////////////////////////////////////////////////////   
            /***********************\
             * 参数部分
            \***********************/
                title_parameter=new JLabel("算法参数");
                title_parameter.setBounds(400,30,100,20);
                this.jp_parameter=new JPanel();
                this.jp_parameter.setBounds(350, 50, 200, 150);
                this.jp_parameter.setLayout(new GridLayout(4,2));
                    label_parameter=new JLabel[4];
                    label_parameter[0]=new JLabel("目标文件");
                    label_parameter[1]=new JLabel("是否压缩索引");
                    label_parameter[2]=new JLabel("是否排序链表");
                    label_parameter[3]=new JLabel("词条长度");
                    b_infile=new JButton("导入");
                    b_infile.addActionListener(this);      
                    jcb_compress=new JComboBox(str_isornot);
                    jcb_compress.addActionListener(this);
                    jcb_sort=new JComboBox(str_isornot);
                    jcb_sort.addActionListener(this);
                    jcb_len=new JComboBox(this.str_len);
                    jcb_len.addActionListener(this);
                    
                jp_parameter.add(label_parameter[0]);
                jp_parameter.add(b_infile);
                jp_parameter.add(label_parameter[1]);
                jp_parameter.add(jcb_compress);
                jp_parameter.add(label_parameter[2]);
                jp_parameter.add(jcb_sort);
                jp_parameter.add(label_parameter[3]);
                jp_parameter.add(jcb_len);                
            /////////////////////////////////////////////////////////      
            /***********************\
             * 按钮部分
            \***********************/    
                this.b_execute=new JButton("执行");
                this.b_execute.setBounds(340,220,60,30);
                this.b_execute.addActionListener(this);
                this.b_reset=new JButton("重置");
                this.b_reset.setBounds(420,220,60,30);
                this.b_reset.addActionListener(this);
                this.jb_outputfile=new JButton("导出");
                this.jb_outputfile.setBounds(500,220,60,30);
                this.jb_outputfile.addActionListener(this);
                this.jb_outputfile.setVisible(false);
                
                    
            ///////////////////////////////////////////////////////// 
            /***********************\
             * 性能部分
            \***********************/ 
                this.title_performance=new JLabel("算法性能参数");
                this.title_performance.setBounds(400,280,100,20);
                this.jp_performance=new JPanel();
                this.jp_performance.setBounds(350,300,200,150);
                jp_performance.setLayout(new GridLayout(5,2));
                    this.label_performance=new JLabel[5];
                    this.label_performance[0]=new JLabel("消耗时间(ms)");
                    this.label_performance[1]=new JLabel("占用空间(MB)");
                    this.label_performance[2]=new JLabel("树总结点数");
                    this.label_performance[3]=new JLabel("词条个数");
                    this.label_performance[4]=new JLabel("文件字数");
                    this.textfield_performance=new JTextField[5];
                    for(int i=0;i<5;i++){
                        this.textfield_performance[i]=new JTextField();
                        this.textfield_performance[i].setEditable(false);                        
                        jp_performance.add(this.label_performance[i]);
                        jp_performance.add(this.textfield_performance[i]);
                    }
            /////////////////////////////////////////////////////////
            jp.add(jp_performance);
            jp.add(this.title_performance);
            jp.add(this.b_execute);
            jp.add(this.b_reset);
            jp.add(this.jb_outputfile);
            jp.add(this.title_parameter);
            jp.add(jp_parameter);
            jp.add(this.title_output);
            jp.add(sc1);
        this.add(jp);
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setResizable(false); 
    }
    ////////////////////////////////////////////
    CHSTextStatistics cts=null;
    DecimalFormat   dformat   =   new   DecimalFormat( "00.0000");
    /**
     * @param cts 统计类
     * @param e 事件
     * 
     */
    //////////////////////////////////////////////
    public void actionPerformed(ActionEvent e){

        if(e.getSource() instanceof JButton){
            if(e.getSource()==this.b_infile){//如果按钮是导入文件
                if(this.jfc_infile.showOpenDialog(this)==0){//打开文件选择窗口
                    File file=jfc_infile.getSelectedFile(); // 获取所选文件
                    String path_name=file.toString().toLowerCase(); // 存放所选文件的绝对路径和文件名（转换成小写）
                    if(file.exists()){// 如果文件存在
                        try{
                            this.cts=new CHSTextStatistics(path_name); //向统计对象传送路径                           
//                            System.out.println(path_name); // 打印下文件路径加文件名
//                            if(path_name.endsWith(".txt")){ 
//                                Runtime.getRuntime().exec("NOTEPAD "+path_name); //以这里也可以直接用，不用加绝对路径
//                            }
                        }catch(Exception ex){
                            ex.printStackTrace();
                            this.outputtextarea.append("打开目标文件"+path_name+"失败\n");//结果输出
                            return;
                        }
                        this.outputtextarea.append("成功打开目标文件"+path_name+"\n");//结果输出
                    }                    
                }
            }else if(e.getSource()==this.b_execute){//按钮是执行
                if(this.cts==null){
                    this.outputtextarea.append("没有导入文件，请先导入文件"+"\n");//结果输出
                    return;                    
                }
                this.cts.solution(this.outputtextarea);//执行统计算法，并获取算法统计结构
                this.textfield_performance[0].setText(""+this.cts.getTime());//获取算法消耗时间
                this.textfield_performance[1].setText(""+this.cts.getRoom());//获取算法消耗内存空间
                this.textfield_performance[2].setText(""+this.cts.getNumTreeNodes());//获取拼音索引树中结点总数
                this.textfield_performance[3].setText(""+this.cts.getNumEntries());//获取词条总数
                this.textfield_performance[4].setText(""+this.cts.getFileCharacters());//获取文件中字数
                this.jb_outputfile.setVisible(true);
            }else if(e.getSource()==this.b_reset){//按钮是重置 
                this.cts=null;//统计类置空
                this.jcb_compress.setSelectedIndex(0);
                this.jcb_len.setSelectedIndex(0);
                this.jcb_sort.setSelectedIndex(0);
                for(int i=0;i<5;i++){
                    this.textfield_performance[i].setText("");
                }
                this.outputtextarea.setText(""); 
                this.jb_outputfile.setVisible(false);
            }else if(e.getSource()==this.jb_outputfile){
                if(this.jfc_outfile.showSaveDialog(this)==JFileChooser.APPROVE_OPTION){//打开文件选择窗口
                    
                    File file=jfc_outfile.getSelectedFile();
                    String path_name=file.getAbsolutePath()+this.jfc_outfile.getFileFilter().getDescription(); // 存放所选文件的绝对路径和文件名（转换成小写）
//                    Thread saver = new Thread(new FileSaver(file, this.cts.tree.getCHSInfoCSV()));
//                    saver.start();
                    try{
                        this.cts.outputFile(path_name);
//                            this.cts=new CHSTextStatistics(path_name); //向统计对象传送路径                           
//                            System.out.println(path_name); // 打印下文件路径加文件名
//                            if(path_name.endsWith(".txt")){ 
//                                Runtime.getRuntime().exec("NOTEPAD "+path_name); //以这里也可以直接用，不用加绝对路径
//                            }
                    }catch(Exception ex){
                            ex.printStackTrace();
                            this.outputtextarea.append("存储文件"+path_name+"失败\n");//结果输出
                            return;
                    }
                    this.outputtextarea.append("成功保存文件"+path_name+"\n");//结果输出                    
                }
            }
        }else{
            if(e.getSource()==this.jcb_compress){
                if(this.cts==null){
                    this.outputtextarea.append("请先导入文件\n");
                    return;
                }
                if(this.jcb_compress.getSelectedItem().toString().equals("是")){
                    this.cts.setCompress(true);
                }else{
                    this.cts.setCompress(false);
                }
                this.outputtextarea.append("设置索引压缩方式：\n\t"
                        +jcb_compress.getSelectedItem().toString()+"\n");
            }else if(e.getSource()==this.jcb_len){
                if(this.cts==null){
                    this.outputtextarea.append("请先导入文件\n");
                    return;
                }
                this.cts.setEntryLen(this.jcb_len.getSelectedIndex()+1);                
                this.outputtextarea.append("设置词条长度：\n\t"+jcb_len.getSelectedItem().toString()+"\n");
            }else if(e.getSource()==this.jcb_sort){
                if(this.cts==null){
                    this.outputtextarea.append("请先导入文件\n");
                    return;
                }
                if(this.jcb_sort.getSelectedItem().toString().equals("是")){
                    this.cts.setDLLSort(true);
                }else{
                    this.cts.setDLLSort(false);
                }               
                this.outputtextarea.append("设置是否链表排序：\n\t"+jcb_sort.getSelectedItem().toString()+"\n");
            }
        }
    }
    public static void main( String[ ] args ){

        // TODO Auto-generated method stub
//        new S_word("G:\\cys_document\\archives of Wang\\射雕英雄传.txt","G:\\cys_document\\archives of Wang\\射雕英雄传去广告.txt").solution();
//        t27.DLinkedListCHS dl=new t27.DLinkedListCHS();
//        dl.add2List("哈哈");
//        dl.add2List("你好");
//        dl.add2List("好");
//        dl.add2List("你好");
//        
//        dl.add2List("好");
//        dl.add2List("你好");
//        dl.add2List("ha");
//        
//        
//        dl.print_list();
//        System.out.print(java.lang.Character.toString('').matches("[\\u4E00-\\u9FA5]+"));
//        new CHSTextStatistics("g:\\chs.txt").solution();
        new Statistics_panel();       
        
        
      //首先是创建JFileChooser 对象，里面带个参数，表示默认打开的目录，这里是默认打开当前文件所在的目录。
        
        
        
        
    }
}
