﻿package t27;

public class T27node{
    
    public String word;//结点字
    public int frequent;//结点频度    
    public T27node[] child=new T27node[27];//孩子结点
    public DLinkedListCHS dllCHS=null;
    
    public T27node(){
        this.word="";
        this.frequent=-1;    
    }
    public T27node(T27node current, char c)
    {
        current.child[mapchar(c)]=this;
        this.frequent=0;
        this.word="";
        for(int i=0;i<27;i++){
            this.child[i]=null;
        }
    }
    static int mapchar(char c)//
    {
        switch(c){
            case ' ':
                return 0;
            default:
                return (int)(c-'a'+1);
        }
    }
}
