﻿package t27;

import java.io.BufferedWriter;

import statistics.TextAreaMenu;

public class DLinkedListCHS{
    // 节点类Node

    private static class Node{
        String value=null;
        int frequent=0;        
        Node   prev =null;
        Node   next =null;
        
        Node(String v){
            frequent=0;
            value=v;
        }
        Node(String v,int i){
            frequent=i;
            value=v;
        }
        public String toString(){
            return value.toString();
        }
    }

    private Node head =new Node(null); // 头节点
    private int  size=0;                // 链表大小

    // 以下是接口方法
    ////////////////////////////////////////
    private Node location=null;
    private boolean locate_list(String v){
        if(this.size==0){
            return false;
        }
        location=this.head;
        for(int i=0;i<this.size;i++){
            location=location.next;
            if(location.value.equals(v)){
                return true;
            }
        }
        return false;
    }
    public void add2List(String v){
//        if(v.equals("作的")){
//            System.out.println();
//        }
        if(locate_list(v)){
            location.frequent++;
            if(location!=this.head.next&&location.prev.frequent<location.frequent){
                location.prev.next=location.next;
                location.next.prev=location.prev;
                Node position=location.prev;
                while((position.frequent<location.frequent)&&(position!=this.head.next)){
                    position=position.prev;
                }
                if(position==this.head.next){
                    if(position.frequent<location.frequent){
                        location.prev=this.head.next.prev;
                        location.next=this.head.next;
                        location.prev.next=location;
                        location.next.prev=location;
                        this.head.next=location;
                    }else{
                        location.prev=position;
                        location.next=position.next;
                        position.next=location;
                        location.next.prev=location;
                    }  
                }else{
                    location.prev=position;
                    location.next=position.next;
                    position.next=location;
                    location.next.prev=location;
                }                
            }
        }else{            
            Node newnode=new Node(v,1);
            if(this.head.next==null){//空链表状态
                this.head.next=newnode;
                newnode.prev=newnode;
                newnode.next=newnode;
            }else{
                newnode.prev=this.head.next.prev;
                newnode.next=this.head.next;
                this.head.next.prev.next=newnode;
                this.head.next.prev=newnode;
            }
            this.size++;
        }
    }
    public void add2List_notsort(String v){
        if(locate_list(v)){
            location.frequent++;
        }else{
            Node newnode=new Node(v,1);
            if(this.head.next==null){//空链表状态
                this.head.next=newnode;
                newnode.prev=newnode;
                newnode.next=newnode;
            }else{
                newnode.prev=this.head.next.prev;
                newnode.next=this.head.next;
                this.head.next.prev.next=newnode;
                this.head.next.prev=newnode;
            }
            this.size++;
        }
    }
    /////////////////////////////////////////
    public void print_list(TextAreaMenu tam){
        if(size==0) return ;
        Node position=this.head.next;
        for(int i=0;i<size;i++){
//            System.out.println("第"+i+"个结点 word="+position.value+" frequent="+position.frequent);
            tam.append(position.value+"\t"+position.frequent+"\n");
            position=position.next;
        } 
    }
    public void getCSV_list(BufferedWriter bw) throws Exception{
        if(size==0) return;
        Node position=this.head.next;
        for(int i=0;i<size;i++){
            bw.write(position.value+","+position.frequent+"\r\n");
            position=position.next;
        } 
    }
    public int size(){
        return size;
    } 
    static public boolean isChinese( char c ){//判断字符是否为汉字
        return java.lang.Character.toString(c).matches("[\\u4E00-\\u9FA5]+");
    }
}
