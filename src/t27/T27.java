﻿package t27;
import java.io.BufferedWriter;
import statistics.TextAreaMenu;

public class T27{
    
    private T27node head=null;//树头结点
    
    public void getCHSInfoCSV(BufferedWriter bw) throws Exception{//返回结点中信息 用逗号分开
        this.getCSV_CHS(bw,this.head);
    }
    private void getCSV_CHS(BufferedWriter bw,T27node p) throws Exception{
        if(p==null)
            return;         
        for(int i=0;i<27;i++){
            if(p.child[i]==null)
                continue;
            getCSV_CHS(bw,p.child[i]);
        }
        if(p.frequent>0){
            p.dllCHS.getCSV_list(bw);
        };
    }
    
    private boolean isDLLSort=true;
    public void setDLLSort(boolean isornot){
        this.isDLLSort=isornot;
    }
////////////////////locate 函数参数////////////////////////////////   
    private T27node p_temp=null;//locate结点 
    private int depth=-1;//locate结点深度
/////////////////////////////////////////////////////////////////
    
////////////////////add2Tree_compressed 函数参数//////////////////
    private T27node neareat_ontop=null;//返回上面最近的结点
    private int depth_of_neareast_ontop=-1;//返回上面最近结点的梯度
    static private enum Relationship {BOTHER,MYSELF,D_ANCESTOR,B_ANCESTOR,D_OFFSPRING,B_OFFSPRING};
    private Relationship m_rs_Of_a_word;//关系
////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
    private long m_num_emptynodes=0;
    private long m_num_allnodes=0;  
    private long m_num_DLLsize=0;
////////////////////////////////////////////////////////////////
    
    public T27(){
        this.head=new T27node(); 
        this.m_num_emptynodes=0;
        this.m_num_allnodes=0;
    }
    public long getNumAllNodes(){
        this.m_num_allnodes=0;
        get_info(this.head);
        return this.m_num_allnodes;
    }
    public long getNumEmptyNodes(){
        this.m_num_emptynodes=0;        
        get_info(this.head);
        return this.m_num_emptynodes;
    }
    public long getDLLsize(){        
        this.m_num_DLLsize=0;
        get_info(this.head);
        return this.m_num_DLLsize;
    }
    public void print_tree_CHS(TextAreaMenu tam){
         this.printT_CHS(this.head,tam);
    }
    public String print_tree(){
        return this.printT(this.head);
    }
    private void printT_CHS(T27node p,TextAreaMenu tam){
        if(p==null)
            return ; 
        for(int i=0;i<27;i++){
            if(p.child[i]==null)
                continue;
            printT_CHS(p.child[i],tam);
        }
        if(p.frequent>0){
//            System.out.println(p.word+"\t"+p.frequent);
            tam.append("拼音索引："+p.word+"\t所含字数"+p.frequent+"\n");
            p.dllCHS.print_list(tam);
        }
    }
    private String printT(T27node p){
        if(p==null)
            return "";
        String result="";
        for(int i=0;i<27;i++){
            if(p.child[i]==null)
                continue;
            result+=printT(p.child[i]);
        }
        if(p.frequent>0){
            System.out.println(p.word+"\t"+p.frequent);
            result+=(p.word+"\t"+p.frequent);
        }
        return result;
    }
    private void get_info(T27node p){
        if(p==null)
            return; 
        for(int i=0;i<27;i++){
            if(p.child[i]==null)
                continue;
            get_info(p.child[i]);
        }
        if(p.frequent==0){
            this.m_num_emptynodes++;
        }
        this.m_num_allnodes++;
        if(p.dllCHS!=null){
            this.m_num_DLLsize+=p.dllCHS.size();
        }
    }    
    public void analyse_node()
    {
        this.m_num_emptynodes=0;
        this.m_num_allnodes=0;
        get_info(this.head);
        System.out.println("27叉树中总结点数：\t"+this.m_num_allnodes);
        System.out.println("有效结点总数：\t"+(this.m_num_allnodes-this.m_num_emptynodes));
        System.out.println("空结点数：\t"+this.m_num_emptynodes);
        System.out.println("结点空余率：\t"+(double)this.m_num_emptynodes/this.m_num_allnodes*100+"%");
        System.out.println("空结点浪费内存空间：\t"+(double)136/1000000+"MB");
        
    }
    
    private int num_identicalChar(String str1, String str2)
    {
        int i=0;
        for(;i<str1.length()&&i<str2.length();i++){
            if(str1.charAt(i)!=str2.charAt(i))
                break;
        }
        return i-1;
    }
    private boolean locate_tree_compressed(String a_word){
        /*
                    该函数返回三个数值： 1 bool 函数返回的。2 p_temp 指针。3 depth 
                    p_temp、depth 在函数返回true时无用
                    当返回false 时，p_temp指向最接近a_word的a_word的祖先，depth表示其深度
        */
        T27node p_temp=this.head;
        T27node temp_pointer=null;
        //cout<<a_word<<endl;
        //cout<<"a_word.length()="<<a_word.length()<<endl;
        //if(a_word=="you")
//        //  cout<<"wait";
//        if(a_word.equals("b"))
//            System.out.println();
        for(int i=0;i<a_word.length();){
            //cout<<"a_word.charAt(i)="<<a_word.charAt(i)<<endl;
            //cout<<"T27node.mapchar(a_word.charAt(i))="<<T27node.mapchar(a_word.charAt(i))<<endl;
            temp_pointer=p_temp.child[T27node.mapchar(a_word.charAt(i))];
            if(temp_pointer==null){
                //找到路径上的null
                this.neareat_ontop=p_temp;         
                this.depth_of_neareast_ontop=i;
                this.m_rs_Of_a_word=Relationship.D_ANCESTOR;//是直系祖先
                return false;
            }else{
                if(temp_pointer.word.length()>a_word.length()){
                    //是直系后代，或者旁系后代
                    if(temp_pointer.word.substring(0,a_word.length()).equals(a_word)){
                        //是直系后代，新加入链条
                        this.neareat_ontop=p_temp;
                        this.depth_of_neareast_ontop=i;
                        this.m_rs_Of_a_word=Relationship.D_OFFSPRING;
                        return false;           
                    }else{
                        //是旁系后代，生成最近共同祖先为根的空字树
                        this.neareat_ontop=p_temp;
                        this.depth_of_neareast_ontop=i;
                        this.m_rs_Of_a_word=Relationship.B_OFFSPRING;
                        return false;
                    }

                }else if(temp_pointer.word.length()<a_word.length()){
                    //还是直系祖先,或是旁系祖先
                    if(a_word.substring(0,temp_pointer.word.length()).equals(temp_pointer.word)){
                        //是直系祖先
                        p_temp=p_temp.child[T27node.mapchar(a_word.charAt(i))];
                        i=temp_pointer.word.length();
                        continue;
                    }else{
                        //是旁系祖先，生成最近共同祖先为根的空字树
                        this.neareat_ontop=p_temp;
                        this.depth_of_neareast_ontop=i;
                        this.m_rs_Of_a_word=Relationship.B_ANCESTOR;
                        return false;
                    }
                }else{
                    //是兄弟，或者是本身
                    if(temp_pointer.word.equals(a_word)){
                        //是本身
                        this.neareat_ontop=temp_pointer;
                        this.depth_of_neareast_ontop=i;
                        this.m_rs_Of_a_word=Relationship.MYSELF;
                        return true;
                    }else{
                        //是兄弟
                        this.neareat_ontop=p_temp;
                        this.depth_of_neareast_ontop=i;
                        this.m_rs_Of_a_word=Relationship.BOTHER;
                        return false;
                    }
                }           
            }       
        }//for       
        return false;            
    }    
    public boolean add2Tree_compressed(String a_word, String chsword){
        T27node p_temp_new=null;
        T27node p_temp_anothertree=null;
        int idcal_position=-1;
        //if(a_word=="you")
        //  cout<<"you";
        if(this.locate_tree_compressed(a_word)){
            
            this.neareat_ontop.frequent++;
            
            if(this.neareat_ontop.word.equals(""))
                this.neareat_ontop.word=a_word;
            
            if(this.neareat_ontop.dllCHS==null){//如果链表中mei有
                this.neareat_ontop.dllCHS=new DLinkedListCHS();                
            }
            
            if(this.isDLLSort){
                this.neareat_ontop.dllCHS.add2List(chsword);
            }else{
                this.neareat_ontop.dllCHS.add2List_notsort(chsword);
            }
        }else{
        //  if(p_temp==this.treehead){
        //      cout<<"frome head"<<endl;
        //  }
        //  cout<<"depth="<<depth<<endl;
            switch(this.m_rs_Of_a_word){
            case BOTHER:
                p_temp_anothertree=
                    this.neareat_ontop.child[T27node.mapchar(a_word.charAt(this.depth_of_neareast_ontop))];//存储兄弟树
                p_temp_new=
                    new T27node(this.neareat_ontop,a_word.charAt(this.depth_of_neareast_ontop));
                p_temp_new.frequent=0;
                idcal_position=this.num_identicalChar(a_word,p_temp_anothertree.word);
                p_temp_new.word=a_word.substring(0,idcal_position+1);
                p_temp_new.child[T27node.mapchar(p_temp_anothertree.word.charAt(idcal_position+1))]
                    =p_temp_anothertree;
                p_temp_new=new T27node(p_temp_new,a_word.charAt(idcal_position+1));
                p_temp_new.frequent=1;
                p_temp_new.word=a_word;
                p_temp_new.dllCHS=new DLinkedListCHS();
                p_temp_new.dllCHS.add2List(chsword);
                break;
            case D_ANCESTOR:
                p_temp_new
                    =new T27node(this.neareat_ontop,a_word.charAt(this.neareat_ontop.word.length()));
                p_temp_new.frequent=1;
                p_temp_new.word=a_word;
                p_temp_new.dllCHS=new DLinkedListCHS();
                p_temp_new.dllCHS.add2List(chsword);
                break;
            case B_ANCESTOR:
                p_temp_anothertree=
                    this.neareat_ontop.child[T27node.mapchar(a_word.charAt(this.depth_of_neareast_ontop))];//存储兄弟树
                p_temp_new=
                    new T27node(this.neareat_ontop,a_word.charAt(this.depth_of_neareast_ontop));
                p_temp_new.frequent=0;
                idcal_position=this.num_identicalChar(p_temp_anothertree.word,a_word);
                p_temp_new.word=p_temp_anothertree.word.substring(0,idcal_position+1);
                p_temp_new.child[T27node.mapchar(p_temp_anothertree.word.charAt(idcal_position+1))]
                    =p_temp_anothertree;
                p_temp_new=new T27node(p_temp_new,a_word.charAt(idcal_position+1));
                p_temp_new.frequent=1;
                p_temp_new.word=a_word;
                p_temp_new.dllCHS=new DLinkedListCHS();
                p_temp_new.dllCHS.add2List(chsword);
                break;
            case D_OFFSPRING:
                p_temp_anothertree=
                    this.neareat_ontop.child[T27node.mapchar(a_word.charAt(this.depth_of_neareast_ontop))];//存储兄弟树
                p_temp_new
                    =new T27node(this.neareat_ontop,a_word.charAt(this.depth_of_neareast_ontop));
                p_temp_new.frequent=1;
                p_temp_new.word=a_word;
                p_temp_new.dllCHS=new DLinkedListCHS();
                p_temp_new.dllCHS.add2List(chsword);
                p_temp_new.child[T27node.mapchar(p_temp_anothertree.word.charAt(a_word.length()))]
                    =p_temp_anothertree;
                break;
            case B_OFFSPRING:
                p_temp_anothertree=
                    this.neareat_ontop.child[T27node.mapchar(a_word.charAt(this.depth_of_neareast_ontop))];//存储兄弟树
                p_temp_new=
                    new T27node(this.neareat_ontop,a_word.charAt(this.depth_of_neareast_ontop));
                p_temp_new.frequent=0;
                idcal_position=this.num_identicalChar(p_temp_anothertree.word,a_word);
                p_temp_new.word=p_temp_anothertree.word.substring(0,idcal_position+1);
                p_temp_new.child[T27node.mapchar(p_temp_anothertree.word.charAt(idcal_position+1))]
                    =p_temp_anothertree;
                p_temp_new=new T27node(p_temp_new,a_word.charAt(idcal_position+1));
                p_temp_new.frequent=1;
                p_temp_new.word=a_word;
                p_temp_new.dllCHS=new DLinkedListCHS();
                p_temp_new.dllCHS.add2List(chsword);
                break;
            case MYSELF:
                break;
            }
        }
    //  this.print_tree();
        return true;
    } 
    public boolean add2Tree_compressed(String a_word){
        T27node p_temp_new=null;
        T27node p_temp_anothertree=null;
        int idcal_position=-1;
        //if(a_word=="you")
        //  cout<<"you";
        if(this.locate_tree_compressed(a_word)){
            this.neareat_ontop.frequent++;
            if(this.neareat_ontop.word.equals(""))
                this.neareat_ontop.word=a_word;            
        }else{
        //  if(p_temp==this.treehead){
        //      cout<<"frome head"<<endl;
        //  }
        //  cout<<"depth="<<depth<<endl;
            switch(this.m_rs_Of_a_word){
            case BOTHER:
                p_temp_anothertree=
                    this.neareat_ontop.child[T27node.mapchar(a_word.charAt(this.depth_of_neareast_ontop))];//存储兄弟树
                p_temp_new=
                    new T27node(this.neareat_ontop,a_word.charAt(this.depth_of_neareast_ontop));
                p_temp_new.frequent=0;
                idcal_position=this.num_identicalChar(a_word,p_temp_anothertree.word);
                p_temp_new.word=a_word.substring(0,idcal_position+1);
                p_temp_new.child[T27node.mapchar(p_temp_anothertree.word.charAt(idcal_position+1))]
                    =p_temp_anothertree;
                p_temp_new=new T27node(p_temp_new,a_word.charAt(idcal_position+1));
                p_temp_new.frequent=1;
                p_temp_new.word=a_word;
                break;
            case D_ANCESTOR:
                p_temp_new
                    =new T27node(this.neareat_ontop,a_word.charAt(this.neareat_ontop.word.length()));
                p_temp_new.frequent=1;
                p_temp_new.word=a_word;
                break;
            case B_ANCESTOR:
                p_temp_anothertree=
                    this.neareat_ontop.child[T27node.mapchar(a_word.charAt(this.depth_of_neareast_ontop))];//存储兄弟树
                p_temp_new=
                    new T27node(this.neareat_ontop,a_word.charAt(this.depth_of_neareast_ontop));
                p_temp_new.frequent=0;
                idcal_position=this.num_identicalChar(p_temp_anothertree.word,a_word);
                p_temp_new.word=p_temp_anothertree.word.substring(0,idcal_position+1);
                p_temp_new.child[T27node.mapchar(p_temp_anothertree.word.charAt(idcal_position+1))]
                    =p_temp_anothertree;
                p_temp_new=new T27node(p_temp_new,a_word.charAt(idcal_position+1));
                p_temp_new.frequent=1;
                p_temp_new.word=a_word;
                break;
            case D_OFFSPRING:
                p_temp_anothertree=
                    this.neareat_ontop.child[T27node.mapchar(a_word.charAt(this.depth_of_neareast_ontop))];//存储兄弟树
                p_temp_new
                    =new T27node(this.neareat_ontop,a_word.charAt(this.depth_of_neareast_ontop));
                p_temp_new.frequent=1;
                p_temp_new.word=a_word;
                p_temp_new.child[T27node.mapchar(p_temp_anothertree.word.charAt(a_word.length()))]
                    =p_temp_anothertree;
                break;
            case B_OFFSPRING:
                p_temp_anothertree=
                    this.neareat_ontop.child[T27node.mapchar(a_word.charAt(this.depth_of_neareast_ontop))];//存储兄弟树
                p_temp_new=
                    new T27node(this.neareat_ontop,a_word.charAt(this.depth_of_neareast_ontop));
                p_temp_new.frequent=0;
                idcal_position=this.num_identicalChar(p_temp_anothertree.word,a_word);
                p_temp_new.word=p_temp_anothertree.word.substring(0,idcal_position+1);
                p_temp_new.child[T27node.mapchar(p_temp_anothertree.word.charAt(idcal_position+1))]
                    =p_temp_anothertree;
                p_temp_new=new T27node(p_temp_new,a_word.charAt(idcal_position+1));
                p_temp_new.frequent=1;
                p_temp_new.word=a_word;
                break;
            case MYSELF:
                break;
            }
        }
    //  this.print_tree();
        return true;
    }
    private boolean locate_tree(String a_word){
        this.p_temp=this.head;
        int i=0;
        for(i=0;i<a_word.length();i++){
            if(this.p_temp.child[T27node.mapchar(a_word.charAt(i))]==null){
                break;
            }else{
                this.p_temp=this.p_temp.child[T27node.mapchar(a_word.charAt(i))];
            }
        }
        this.depth=i;
        if(i==a_word.length()){
            return true;
        }else{
            return false;
        }
    }
    public boolean add2Tree(String a_word){
        if(this.locate_tree(a_word)){
            this.p_temp.frequent++;
            if(this.p_temp.word.equals("")){
                this.p_temp.word=a_word;
            }
        }else{
            for(int i=depth;i<a_word.length();i++){
                this.p_temp=new T27node(this.p_temp,a_word.charAt(i));
            }
            this.p_temp.frequent++;
            this.p_temp.word=a_word;
        }
        return true;
    }
    public boolean add2Tree(String a_word, String chsword){
        if(this.locate_tree(a_word)){
            this.p_temp.frequent++;
            if(this.p_temp.word.equals("")){
                this.p_temp.word=a_word;
            }
            if(this.p_temp.dllCHS==null){//如果链表中有
                this.p_temp.dllCHS=new DLinkedListCHS();
                this.p_temp.dllCHS.add2List(chsword);
            }else{//如果链表中没有
                this.p_temp.dllCHS.add2List(chsword);
            }
        }else{
            for(int i=depth;i<a_word.length();i++){
                this.p_temp=new T27node(this.p_temp,a_word.charAt(i));
            }
            this.p_temp.frequent++;
            this.p_temp.word=a_word;
            p_temp.dllCHS=new DLinkedListCHS();
            p_temp.dllCHS.add2List(chsword);
        }
        return true;
    }
    
}
