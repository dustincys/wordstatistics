﻿package csvDataAnalyse;

import java.io.BufferedWriter;
import java.io.FileWriter;

public class FrequentReduce extends CSVFILE{
    private FileWriter  fw_outfile;
    private BufferedWriter  bw_outfile;
    public FrequentReduce(String url1,String url2){
        super(url1);
        try{
            this.fw_outfile=new FileWriter(url2);
            this.bw_outfile=new BufferedWriter(this.fw_outfile);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
//    private String getWord(String oneline){
//        int head=oneline.indexOf(",")+1;
//        int end=oneline.indexOf(",",head);
////        System.out.println(oneline.substring(head,end));
//        return oneline.substring(head,end);
//    }
    public void solution(int i){
        try{
            String oneline=null;
            String word=null;
            int No_=1;
            while((oneline=this.get_oneline())!=null){
                if((word=(oneline.substring(0,oneline.indexOf(",")))).length()<2){
                    System.out.println("忽略");
                    continue;
                }
                this.bw_outfile.write(word+"，");
                System.out.println(word+"，");
                if(No_++>=i){
                    break;
                }
            }
            this.bw_outfile.close();
            this.br_infile.close();
            this.fw_outfile.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * @param args
     */
    public static void main( String[ ] args ){
           new FrequentReduce(
                   "G:\\cys_document\\archives of Wang\\军事语料词频统计(去掉长度1).csv",
                   "G:\\cys_document\\archives of Wang\\特征词条.csv"
           ).solution(484);
    }

}
