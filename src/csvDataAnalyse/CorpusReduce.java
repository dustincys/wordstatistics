﻿package csvDataAnalyse;

import java.io.BufferedWriter;
import java.io.FileWriter;

public class CorpusReduce extends CSVFILE{
    protected FileWriter  fw_outfile;
    protected BufferedWriter  bw_outfile;
    protected CSVFILE sourcefile1;
    
    public CorpusReduce(String url1,String url2){
        super(url1);
        try{
            this.fw_outfile=new FileWriter(url2);
            this.bw_outfile=new BufferedWriter(this.fw_outfile);
        }catch(Exception e){
            e.printStackTrace();
        }
        // TODO Auto-generated constructor stub
    }
    public void solution(){
        try{
            String oneline;
            int No_=1;
            while((oneline=this.get_oneline())!=null){
//                if(oneline.contains("[")){
//                    oneline=oneline.replace("[","");
//                }
//                if(oneline.contains("]")){
//                    oneline=oneline.replace("]","");
//                }
                if(oneline.contains("【")&&oneline.contains("】")){
                    oneline=oneline.replace(
                            oneline.substring(oneline.indexOf("【文件名:"),oneline.length()),"");
                }
                if(oneline.length()<="99633:  　　前[海军]人员致罗斯福总统　　　　　　　　　　　　1942年7月6日                ".length()){
                    continue;
                }
                this.bw_outfile.write(oneline+"\r\n");
                System.out.println(oneline);No_++;
                System.out.println("过滤后条数：\t"+No_);
            }
            this.bw_outfile.close();
            this.br_infile.close();
            this.fw_outfile.close();
            this.br_infile.close();
        }catch(Exception e){
            e.printStackTrace();
        }
        
    }

    /**
     * @param args
     */
    public static void main( String[ ] args ){
        new CorpusReduce(
                "G:\\cys_document\\archives of Wang\\1.txt",
                "G:\\cys_document\\archives of Wang\\军事语料(去标签).txt"
        ).solution();
        // TODO Auto-generated method stub

    }

}
