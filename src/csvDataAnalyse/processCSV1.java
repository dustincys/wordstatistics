﻿package csvDataAnalyse;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class processCSV1 {
    private FileWriter  fw_outfile;
    private BufferedWriter  bw_outfile;
    private CSVFILE sourcefile1,sourcefile2;
    private Connection                      con;
    private ResultSet                       rs;
    private Statement                       stmt;
    private Statement                       stmt1;
    String databasename=null;
    
    public processCSV1(String url1,String url2,String url3,String _databasename){
        this.databasename=_databasename;
        try{
            this.sourcefile1=new CSVFILE(url1);
            this.sourcefile2=new CSVFILE(url2);
            this.fw_outfile=new FileWriter(url3);
            this.bw_outfile=new BufferedWriter(this.fw_outfile);
        }catch(Exception e){e.printStackTrace();}
        
        try{
            Class.forName( "com.mysql.jdbc.Driver" );
            DriverManager.registerDriver( new com.mysql.jdbc.Driver( ) );
            String dbUrl= "jdbc:mysql://localhost:3306/entry";
            String dbUser= "root";
            String dbPwd= "kainan";
            con= java.sql.DriverManager.getConnection( dbUrl, dbUser, dbPwd );
            stmt= con.createStatement( );
            stmt1= con.createStatement( );            
        }catch(Exception e){
            e.printStackTrace( );
            System.exit( 0 );
        }
        
        try{
            stmt.executeUpdate("ALTER DATABASE`entry`DEFAULT CHARACTER SET utf8 COLLATE utf8_bin");
            stmt.executeUpdate( "drop table `entry`.`"+this.databasename+"`");  
            stmt.executeUpdate(
                    "CREATE  TABLE `entry`.`"+this.databasename+"` ("
                    +"`No_` INT NOT NULL ,"
                    +"`word` varchar(100) NULL,"
                    +"PRIMARY KEY (`No_`) );" 
                    );            
        }catch(Exception e){
            e.printStackTrace();
        }
        // TODO Auto-generated constructor stub
    }
    public void solution(){
        try{
            String oneline=null;
            String oneword=null;
            int No_=1;
            while((oneline=this.sourcefile1.get_oneline())!=null){                
                oneword=this.getWord(oneline);
                if(oneword.length()<2){
                    System.out.print("忽略");
                    continue;
                }
                rs=stmt1.executeQuery("SELECT `word` FROM `entry`.`"+this.databasename+"` WHERE `word`='"+oneword+"';");
                rs.last();
                int row=rs.getRow();
                if(row!=0){
                    continue;
                }else{               
                    stmt1.executeUpdate("INSERT INTO `entry`.`words` (`No_`, `word`) VALUES (5, '文章');");
                    this.bw_outfile.write(No_+","+oneword+"\n");
                    System.out.println("\nNo"+No_+"\t"+oneword);
                    No_++;
                }
            }
            while((oneline=this.sourcefile2.get_oneline())!=null){
                oneword=this.getWord(oneline);
                if(oneword.length()<2){
                    System.out.print("忽略"+oneword.length());
                    continue;
                }
                rs=stmt1.executeQuery("SELECT `word` FROM `entry`.`"+this.databasename+"` WHERE `word`='"+oneword+"';");
                rs.last();
                if(rs.getRow()!=0){
                    continue;
                }else{               
                    stmt1.executeUpdate("INSERT INTO `entry`.`words` (`No_`, `word`) VALUES ("+No_+", '"+oneword+"');");
                    this.bw_outfile.write(No_+","+oneword+"\n");
                    System.out.println("\nNo"+No_+"\t"+oneword);
                    No_++;
                }
            }
            this.bw_outfile.close();
            this.fw_outfile.close();
            this.stmt.close();
            this.stmt1.close();
            this.con.close();
        }catch(Exception e){e.printStackTrace();}
    }
    private String getWord(String oneline){
        int head=oneline.indexOf(",")+1;
        int end=oneline.indexOf(",",head);
//        System.out.println(oneline.substring(head,end));
        return oneline.substring(head,end);
    }
    /**
     * @param args
     */
    public static void main( String[ ] args ){
        new processCSV1(
                "G:\\cys_document\\智能在线咨询系统\\计算科学语料\\#词频统计#2011-10-05-2.csv",
                "G:\\cys_document\\智能在线咨询系统\\生命科学语料\\#词频统计#2011-10-05.csv",
                "G:\\cys_document\\智能在线咨询系统\\计算机、生命科学词汇表.csv",
                "words"
        ).solution();
    }

}
