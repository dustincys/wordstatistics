﻿package csvDataAnalyse;

import java.io.BufferedWriter;
import java.io.FileWriter;

public class process3{
    private FileWriter  fw_outfile;
    private BufferedWriter  bw_outfile;
    private CSVFILE sourcefile1;
   
    
    public process3(String url1,String url2){
        try{
            this.sourcefile1=new CSVFILE(url1);
            this.fw_outfile=new FileWriter(url2);
            this.bw_outfile=new BufferedWriter(this.fw_outfile);
        }catch(Exception e){e.printStackTrace();}    
    }
    public void solution(){
        try{
            String oneline1=null;
            String oneword1=null;
            String oneline2="";
            String oneword2="";
            while((oneline1=this.sourcefile1.get_oneline())!=null){
                oneword1=this.getWord(oneline1);
                if(!oneword1.equals(oneword2)){
                    oneword2=oneword1;
                    oneline2=oneline1;
                    continue;
                }else{
                    this.bw_outfile.write(oneline1+","+oneline2+"\n");  
                    System.out.println(oneline1);
                }
            }
            this.bw_outfile.close();
            this.fw_outfile.close();
        }catch(Exception e){}
    }
    private String getWord(String oneline){
        int head=oneline.indexOf(",")+1;
        int end=oneline.indexOf(",",head);
//        System.out.println(oneline.substring(head,end));
        return oneline.substring(head,end);
    }
    public static void main( String[ ] args ){
        new process3(
                "G:\\cys_document\\智能在线咨询系统\\计算机、生命科学词汇表.csv",
                "G:\\cys_document\\智能在线咨询系统\\词语同时出现,csv"
        ).solution();
    }
}
