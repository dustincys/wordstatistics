﻿package csvDataAnalyse;

import java.io.BufferedWriter;
import java.io.FileWriter;


public class processCSV2 {
    private FileWriter  fw_outfile;
    private BufferedWriter  bw_outfile;
    private CSVFILE sourcefile1,sourcefile2;
   
    
    public processCSV2(String url1,String url2,String url3){
        try{
            this.sourcefile1=new CSVFILE(url1);
            this.sourcefile2=new CSVFILE(url2);
            this.fw_outfile=new FileWriter(url3);
            this.bw_outfile=new BufferedWriter(this.fw_outfile);
        }catch(Exception e){e.printStackTrace();}    
    }
    public void solution(){
        try{
            String oneline=null;
            String oneword=null;
            int No_=1;
            while((oneline=this.sourcefile1.get_oneline())!=null){                
                oneword=this.getWord(oneline);
                if(oneword.length()<2){
                    System.out.print("忽略");
                    continue;
                }                
                    this.bw_outfile.write(oneline+"\n");
                    System.out.println("\nNo"+No_+"\t"+oneword);
                    No_++;
            }            
            while((oneline=this.sourcefile2.get_oneline())!=null){                
                oneword=this.getWord(oneline);
                if(oneword.length()<2){
                    System.out.print("忽略");
                    continue;
                }                
                    this.bw_outfile.write(oneline+"\n");
                    System.out.println("\nNo"+No_+"\t"+oneword);
                    No_++;
            }            
            this.bw_outfile.close();
            this.fw_outfile.close();            
        }catch(Exception e){e.printStackTrace();}
    }
    private String getWord(String oneline){
        int head=oneline.indexOf(",")+1;
        int end=oneline.indexOf(",",head);
//        System.out.println(oneline.substring(head,end));
        return oneline.substring(head,end);
    }
    /**
     * @param args
     */
    public static void main( String[ ] args ){
        new processCSV2(
                "G:\\cys_document\\智能在线咨询系统\\计算科学语料\\#词频统计#2011-10-05-2.csv",
                "G:\\cys_document\\智能在线咨询系统\\生命科学语料\\#词频统计#2011-10-05.csv",
                "G:\\cys_document\\智能在线咨询系统\\计算机、生命科学词汇表.csv"
        ).solution();
    }

}
