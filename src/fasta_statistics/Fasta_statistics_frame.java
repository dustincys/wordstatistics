﻿package fasta_statistics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
//import java.text.DecimalFormat;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import statistics.TextAreaMenu;

public class Fasta_statistics_frame extends JFrame implements ActionListener{
    public Fasta_statistics_frame(){
        super("fasta序列特征统计工具___yanshuoc@gmail.com");
        try{
            initial();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private static final long serialVersionUID =1L;
    private JPanel            jp;                             // 底板
    private JPanel            jp_Parameter;                   // 参数底板
    private JPanel            jp_Performance;                 // 性能底板
    private TextAreaMenu      tam_OutputTextArea;             // 输出框
    private JScrollPane       jsp_OutputTextArea;             // 输出框滚动条
    private JLabel            jl_title_OutputTextArea,// 输出框标题
            jl_title_Parameter,// 参数标题
            jl_title_Performance;                             // 性能标题
    private JLabel[ ]         jl_Parameter,// 各项参数标题
            jl_Performance;                                   // 各项性能标题
    private JButton           jb_Infile;                      // 导入文件按钮
    private JTextField        jtf_Consistency;                // 一致性输入区域
    private JComboBox         jcb_EachOrAll;                  // 单个/整体统计选项框
    private final String[ ]   str_IsOrNot      ={ "分条", "整体" }; // 单个/整体统计选项
    private final String[ ]   str_IsOrNotSpecialized      ={ "普遍统计", "特定统计" }; 
//    private JPanel            jp_SpecificEntry;               //
    private JComboBox         jcb_SpecificEntry;              //
    private JTextField        jtf_SpecificEntry;              //
    private JTextField        jtf_EntryLen;              //
    
    private JButton           jb_Execute,jb_Reset,jb_Outfile;

    private JTextField        jtf_FastaItemNum;               //
    private JTextField        jtf_Occurrence;                 //
    private JTextField        jtf_MeanFrequence;              //

    private JFileChooser      jfc_Infile,jfc_Outfile;


    private void initial(){
        jfc_Infile=new JFileChooser();
        jfc_Infile.setFileFilter(new javax.swing.filechooser.FileFilter(){
            public boolean accept( File f ){
                return f.getName().toLowerCase().endsWith(".fasta")
                        ||f.isDirectory();
            }
            public String getDescription(){
                return ".fasta";
            }
        });
        jfc_Outfile=new JFileChooser();
        jfc_Outfile.setFileFilter(new javax.swing.filechooser.FileFilter(){
            public boolean accept( File f ){
                return f.getName().toLowerCase().endsWith(".csv")
                        ||f.isDirectory();
            }
            public String getDescription(){
                return ".csv";
            }
        });
        this.setBounds(100, 100, 600, 500);
        jp=new JPanel();
        jp.setBounds(0, 0, 600, 500);
        jp.setLayout(null);
        //////////////////////////////////////////////////////
        /***********************\
         * 显示框部分
        \***********************/
        this.jl_title_OutputTextArea=new JLabel("统计结果");
        this.jl_title_OutputTextArea.setBounds(100,30,100,20);
        this.tam_OutputTextArea=new TextAreaMenu(){
            private static final long serialVersionUID = -2308615404205560110L;  
            public void mouseClicked(MouseEvent e) {  
                if(e.getButton()==MouseEvent.BUTTON1)
                    selectAll();
            } 
        };
        this.tam_OutputTextArea.setEditable(false);
        this.jsp_OutputTextArea=new JScrollPane(this.tam_OutputTextArea); 
        this.jsp_OutputTextArea.setBounds(10, 50, 280, 400);        
        //////////////////////////////////////////////////////
        //////////////////////////////////////////////////////
        /***********************\
         * 参数部分
        \***********************/
        this.jl_title_Parameter=new JLabel("统计参数");
        this.jl_title_Parameter.setBounds(400,30,100,20);
        this.jp_Parameter=new JPanel();
        this.jp_Parameter.setBounds(350, 60, 200, 150);
        this.jp_Parameter.setLayout(new GridLayout(6,2));
            this.jl_Parameter=new JLabel[6];
            this.jl_Parameter[0]=new JLabel("导入文件");
            this.jl_Parameter[1]=new JLabel("一致性(%)");
            this.jl_Parameter[2]=new JLabel("是否分条统计");
            this.jl_Parameter[3]=new JLabel("统计方式");
            this.jl_Parameter[4]=new JLabel("输入特定词条");
//            this.jl_Parameter[4].setVisible(false);
            this.jl_Parameter[5]=new JLabel("统计词条长度");
            this.jb_Infile=new JButton("导入");
            this.jb_Infile.addActionListener(this);
            this.jtf_Consistency=new JTextField("40");
//            this.jtf_Consistency.addActionListener(this);
            this.jcb_EachOrAll=new JComboBox(this.str_IsOrNot);
            this.jcb_EachOrAll.setSelectedIndex(1);
            this.jcb_EachOrAll.addActionListener(this);
//            this.jp_SpecificEntry=new JPanel();
//            this.jp_SpecificEntry.setLayout(new GridLayout(1,2));
            this.jcb_SpecificEntry=new JComboBox(this.str_IsOrNotSpecialized);
            this.jcb_SpecificEntry.addActionListener(this);
            this.jtf_SpecificEntry=new JTextField();
            this.jtf_SpecificEntry.setEnabled(false);
//            this.jtf_SpecificEntry.addActionListener(this);
            this.jtf_EntryLen=new JTextField("1");
//            this.jtf_EntryLen.addActionListener(this);
//            this.jp_SpecificEntry.add(this.jcb_SpecificEntry);
//            this.jp_SpecificEntry.add(this.jtf_SpecificEntry);
        this.jp_Parameter.add(this.jl_Parameter[0]);
        this.jp_Parameter.add(this.jb_Infile);
        this.jp_Parameter.add(this.jl_Parameter[1]);
        this.jp_Parameter.add(this.jtf_Consistency);
        this.jp_Parameter.add(this.jl_Parameter[2]);
        this.jp_Parameter.add(this.jcb_EachOrAll);
        this.jp_Parameter.add(this.jl_Parameter[3]);
        this.jp_Parameter.add(this.jcb_SpecificEntry);
        this.jp_Parameter.add(this.jl_Parameter[4]);
        this.jp_Parameter.add(this.jtf_SpecificEntry);
        this.jp_Parameter.add(this.jl_Parameter[5]);
        this.jp_Parameter.add(this.jtf_EntryLen);
        //////////////////////////////////////////////////////
        //////////////////////////////////////////////////////
        /***********************\
         * 按钮部分
        \***********************/
        this.jb_Execute=new JButton("执行");
        this.jb_Execute.setBounds(340,220,60,30);
        this.jb_Execute.addActionListener(this);
        this.jb_Reset=new JButton("重置");
        this.jb_Reset.setBounds(420,220,60,30);
        this.jb_Reset.addActionListener(this);
        this.jb_Outfile=new JButton("导出");
        this.jb_Outfile.setBounds(500,220,60,30);
        this.jb_Outfile.addActionListener(this);
        this.jb_Outfile.setVisible(false);
        //////////////////////////////////////////////////////
        //////////////////////////////////////////////////////
        /***********************\
         * 性能部分
        \***********************/
        this.jl_title_Performance=new JLabel("统计结果参数");
        this.jl_title_Performance.setBounds(400,280,100,20);
        this.jp_Performance=new JPanel();
        this.jp_Performance.setBounds(350,310,200,100);
        this.jp_Performance.setLayout(new GridLayout(3,2));
            this.jl_Performance=new JLabel[3];
            this.jl_Performance[0]=new JLabel("序列条数");
            this.jl_Performance[1]=new JLabel("频数");
            this.jl_Performance[1].setVisible(false);
            this.jl_Performance[2]=new JLabel("频率(%)");
            this.jl_Performance[2].setVisible(false);
            this.jtf_FastaItemNum=new JTextField();
            this.jtf_FastaItemNum.setEditable(false);
            this.jtf_Occurrence=new JTextField();
            this.jtf_Occurrence.setVisible(false);            
            this.jtf_Occurrence.setEditable(false);
            this.jtf_MeanFrequence=new JTextField();
            this.jtf_MeanFrequence.setVisible(false);
            this.jtf_MeanFrequence.setEditable(false);
        this.jp_Performance.add(this.jl_Performance[0]);
        this.jp_Performance.add(this.jtf_FastaItemNum);
        this.jp_Performance.add(this.jl_Performance[1]);
        this.jp_Performance.add(this.jtf_Occurrence);
        this.jp_Performance.add(this.jl_Performance[2]);
        this.jp_Performance.add(this.jtf_MeanFrequence);
        //////////////////////////////////////////////////////
        this.jp.add(this.jp_Performance);
        this.jp.add(this.jl_title_Performance);
        this.jp.add(this.jb_Execute);
        this.jp.add(this.jb_Reset);
        this.jp.add(this.jb_Outfile);
        this.jp.add(this.jl_title_Parameter);
        this.jp.add(this.jp_Parameter);
        this.jp.add(this.jsp_OutputTextArea);
        this.jp.add(this.jl_title_OutputTextArea);
    this.add(jp);
    this.setVisible(true);
    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    this.setResizable(false); 
    }
    private fastaStatistics.FastaStatistics fs=null;
    private FileWriter                      fw=null;
    private BufferedWriter                  bw=null;
    public void actionPerformed( ActionEvent e ){
        if(e.getSource() instanceof JButton){
            if(e.getSource()==this.jb_Execute){
                if(!this.isInFile()){
                    this.tam_OutputTextArea.append("请先导入文件\r\n");
                    return;
                }
                if((this.jcb_EachOrAll.getSelectedIndex()==0)&&!this.isOutFile()){
                    this.tam_OutputTextArea.append("请先选定导出文件\r\n");
                    return;
                }
                this.onButtonExecute();
            }else if(e.getSource()==this.jb_Infile){
                this.onButtonInfile();
            }else if(e.getSource()==this.jb_Outfile){
                if(!this.isInFile()){
                    this.tam_OutputTextArea.append("请先导入文件\r\n");
                    return;
                }
                this.onButtonOutfile();
            }else if(e.getSource()==this.jb_Reset){
                this.onButtonReset();
            }
        }else if(e.getSource() instanceof JComboBox){            
            if(e.getSource()==this.jcb_EachOrAll){
                if(this.fs!=null) this.fs.setboolEachOrAll(this.jcb_EachOrAll.getSelectedIndex()==0);
                this.onComponentEachOrAll(this.jcb_EachOrAll.getSelectedIndex()==1);
            }else if(e.getSource()==this.jcb_SpecificEntry){
                if(this.fs!=null) this.fs.setboolSpecificEntry(this.jcb_SpecificEntry.getSelectedIndex()==1);
                this.onComponentSpecificEntry(this.jcb_SpecificEntry.getSelectedIndex()==1);
            }
        }
    }
    private boolean isInFile(){
        return !(this.fs==null);
    }
    private boolean isOutFile(){
        return (this.fw!=null)&&(this.bw!=null);
    }
    private void onComponentEachOrAll(boolean isornot){
        this.jb_Outfile.setVisible(!isornot);
    }
    private void onComponentSpecificEntry(boolean isornot){
//        this.jl_Parameter[1].setVisible(!isornot);
        this.jtf_Consistency.setEnabled(!isornot);
//        this.jl_Parameter[5].setVisible(!isornot);
        this.jtf_EntryLen.setEnabled(!isornot);
//        this.jl_Parameter[4].setVisible(isornot);
        this.jtf_SpecificEntry.setEnabled(isornot);
        this.jl_Performance[1].setVisible(isornot);
        this.jl_Performance[2].setVisible(isornot);
        this.jtf_Occurrence.setVisible(isornot);
        this.jtf_MeanFrequence.setVisible(isornot);
    }
    private void onComponentReset(){        
        this.jtf_Consistency.setText("40");
        this.jtf_EntryLen.setText("1");
        this.jtf_FastaItemNum.setText("");
        this.jtf_MeanFrequence.setText("");
        this.jtf_Occurrence.setText("");
        this.jtf_SpecificEntry.setText("");
        this.jcb_EachOrAll.setSelectedIndex(1);
        this.jcb_SpecificEntry.setSelectedIndex(0);
        this.tam_OutputTextArea.setText("");
        this.onComponentSpecificEntry(false);
    }
//    DecimalFormat   dformat   =   new   DecimalFormat( ".00");
    private void onButtonExecute(){
        if(!this.setParameter()){
            return;
        }
        if(this.jcb_EachOrAll.getSelectedIndex()==0){//分条显示
            if(this.jcb_SpecificEntry.getSelectedIndex()==0){//普遍
                this.fs.solution(this.tam_OutputTextArea,this.bw);               
                try{
                    this.bw.close();
                    this.fw.close();
                }catch(Exception e){e.printStackTrace();};
            }else{
                this.fs.solution(this.tam_OutputTextArea,this.bw);               
                try{
                    this.bw.close();
                    this.fw.close();
                }catch(Exception e){e.printStackTrace();};
                this.jtf_Occurrence.setText(""+this.fs.getOccurrence());
                this.jtf_MeanFrequence.setText(""+this.fs.getMeanFrequence());
            }
        }else{//整体
            if(this.jcb_SpecificEntry.getSelectedIndex()==0){//普遍
                this.fs.solution();
                this.tam_OutputTextArea.append(this.fs.getTree().print_tree());
    //            this.fs.showResult(tam_OutputTextArea);
                this.jb_Outfile.setVisible(true);
            }else{//特殊
                this.fs.solution();
                this.tam_OutputTextArea.append("完成统计\r\n");
                this.jb_Outfile.setVisible(true);
            }
        }
        this.jtf_MeanFrequence.setText(""+this.fs.getMeanFrequence());
        this.jtf_Occurrence.setText(""+this.fs.getOccurrence());
        this.jtf_FastaItemNum.setText(""+this.fs.getFastaItemNum());
    }
    private boolean setParameter(){
        this.fs.setboolEachOrAll(this.jcb_EachOrAll.getSelectedIndex()==0);
        this.fs.setboolSpecificEntry(this.jcb_SpecificEntry.getSelectedIndex()==1);
        double tempConsistency;
        try{
            tempConsistency=Double.parseDouble(this.jtf_Consistency.getText());
            if(tempConsistency<0||tempConsistency>100){
                this.tam_OutputTextArea.append("输入数值在0~100之间\r\n");
                return false;
            }
        }catch(Exception ex){
            this.tam_OutputTextArea.append("请输入double形数值\r\n");
            return false;
        }
        int tempEntryLen;
        try{
            tempEntryLen=Integer.parseInt(this.jtf_EntryLen.getText());
        }catch(Exception ex){
            this.tam_OutputTextArea.append("请输入整形数值\r\n");
            return false;
        }                
        this.fs.setdoubleConsistency(tempConsistency); 
        this.fs.setintEntryLen(tempEntryLen);
        this.fs.setstrSpecificEntry(this.jtf_SpecificEntry.getText().toLowerCase()); 
        return true;
    }
    private void onButtonInfile(){
        if(this.jfc_Infile.showOpenDialog(this)==0){
            File file=this.jfc_Infile.getSelectedFile(); // 获取所选文件
            String path_name=file.toString().toLowerCase(); // 存放所选文件的绝对路径和文件名（转换成小写）
            if(file.exists()){// 如果文件存在
                try{
                    this.fs=new fastaStatistics.FastaStatistics(path_name); //向统计对象传送路径                           
//                    System.out.println(path_name); // 打印下文件路径加文件名
//                    if(path_name.endsWith(".txt")){ 
//                        Runtime.getRuntime().exec("NOTEPAD "+path_name); //以这里也可以直接用，不用加绝对路径
//                    }
                }catch(Exception ex){
                    ex.printStackTrace();
                    this.tam_OutputTextArea.append("打开目标文件"+path_name+"失败\n");//结果输出
                    return;
                }
                this.tam_OutputTextArea.append("成功打开目标文件"+path_name+"\n");//结果输出
            }      
        }
    }
    private void onButtonOutfile(){
        if(this.jfc_Outfile.showSaveDialog(this)==JFileChooser.APPROVE_OPTION){            
            File file=this.jfc_Outfile.getSelectedFile();
            String path_name=file.getAbsolutePath()+this.jfc_Outfile.getFileFilter().getDescription(); // 存放所选文件的绝对路径和文件名（转换成小写）
//                Thread saver = new Thread(new FileSaver(file, this.cts.tree.getCHSInfoCSV()));
//                saver.start();
            try{
                this.fw=new FileWriter(path_name);
                this.bw=new BufferedWriter(this.fw);// 
                if(this.jcb_EachOrAll.getSelectedIndex()==1){//不是分条统计
                    if(this.jcb_SpecificEntry.getSelectedIndex()==0){
                        this.bw.write(this.fs.getTree().print_tree());//是普遍统计
                    }else{
                        this.bw.write("词条总数,"+this.fs.getFastaItemNum()+"\r\n");
                        this.bw.write("频数,"+this.fs.getOccurrence()+"\r\n");
                        this.bw.write("频率(%),"+this.fs.getMeanFrequence()+"\r\n");                        
                    }
                    this.tam_OutputTextArea.append("成功保存文件"+path_name+"\n");//
                    this.bw.close();
                    this.fw.close();
                }
            }catch(Exception ex){
                ex.printStackTrace();
                this.tam_OutputTextArea.append("存储文件"+path_name+"失败\n");//结果输出
                return;
            }                               
        }        
    }
    private void onButtonReset(){
        this.fs.setReset();  
        this.onComponentReset();
    }
    
    public static void main( String[ ] args ){
        new Fasta_statistics_frame();
    }

}
